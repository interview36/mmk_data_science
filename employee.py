from dataclasses import dataclass
import face_recognition


@dataclass
class Employee:
    id: int
    name: str
    image_link: str
    job_position: str
    id_job_position: int

    def get_image(self):
        img = face_recognition.load_image_file(self.image_link)
        return img
