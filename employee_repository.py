import sqlite3
from sqlite3 import Error

from employee import Employee

PATH_SQL = 'data.sqlite'


def create_connection(path):
    connection = None
    try:
        connection = sqlite3.connect(path)
        print("Connection to SQLite DB successful")
    except Error as e:
        print(f"The error '{e}' occurred")
    return connection


def get_employee(ide: int) -> Employee:
    connection = create_connection(PATH_SQL)
    cursor = connection.cursor()

    query = f'select ' \
            f'name, image_link, job_position, job_position_id ' \
            f'from employees left outer join job_positions ' \
            f'on job_positions.id = employees.job_position_id ' \
            f'where employees.id={ide}'
    cursor.execute(query)
    data = cursor.fetchone()
    if data is not None:
        empl = Employee(id=ide, name=data[0], image_link=data[1], job_position=data[2], id_job_position=data[3])
    else:
        empl = None

    cursor.close()
    connection.close()

    return empl
