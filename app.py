from flask import Flask, request, Response, jsonify
from employee_repository import get_employee
import face_recognition
from os import listdir
import re

app = Flask(__name__)


# просто проверочная функция работы веб сервиса
@app.route('/preflight', methods=['GET'])
def preflight() -> Response:
    return jsonify({'1': 1})


# функции идентификации
@app.route('/check-employee', methods=['POST'])
def check_employee() -> Response:
    img = request.files['image']
    ide = int(request.form['data'])

    # получаем фото предыдущих идентификаций данного сотрудника
    # где имя файла - (id сотрудника)_(попытка идентификации).jpg
    files = listdir('idents')
    files_with_id = list(filter(lambda x: f'{ide}_' in x, files))

    cnt_files = len(files_with_id)

    # сохраняем текущую попытку
    img.save(f'idents/{ide}_{cnt_files + 1}.jpg')

    # считываем из БД сотрудника
    employee = get_employee(ide)
    if employee is None:
        return jsonify({'Error': f'not fined employee ID:{ide} in database'})

    # получаем признаки и сравниваем лица
    rgb_ref_img = employee.get_image()
    ref_face_location = face_recognition.face_locations(rgb_ref_img)
    ref_enc = face_recognition.face_encodings(rgb_ref_img, ref_face_location)

    face = face_recognition.load_image_file(img)
    face_locs = face_recognition.face_locations(face)

    if len(face_locs) != 1:
        return jsonify({'Error': 'more than one person was found or no one',
                        'Face numbers': len(face_locs),
                        'Name': employee.name,
                        'id_employee': employee.id,
                        'job_position': employee.job_position,
                        'id_job_position': employee.id_job_position,
                        'tolerance': 0.6,
                        'distance': 1})

    face_enc = face_recognition.face_encodings(face, face_locs)
    face_distances = face_recognition.face_distance(ref_enc, face_enc[0])
    matches = face_recognition.compare_faces(ref_enc, face_enc[0])

    return jsonify({'Success': bool(matches[0]),
                    'Name': employee.name,
                    'id_employee': employee.id,
                    'job_position': employee.job_position,
                    'id_job_position': employee.id_job_position,
                    'tolerance': 0.6,
                    'distance': face_distances[0]})


# запрос фото последней идентификации сотрудника с указанным ID
@app.route('/identification', methods=['GET'])
def request_identification() -> Response:
    ide = int(request.form['id'])

    # получаем названия файлов
    files = listdir('idents')
    # отбираем из списка только файлы, принадлежащие данному сотруднику
    files_with_id = list(filter(lambda x: f'{ide}_' in x, files))
    cnt_files = len(files_with_id)

    if cnt_files != 0:
        # сортируем по номеру попытки идентификации данного сотрудника
        files_with_id.sort(key=lambda x: int(re.findall(r'\d+', x)[1]))
        return jsonify({'id_file': f'idents/{files_with_id[cnt_files - 1]}'})
    else:
        return jsonify({'Error': "can't find this employee ID"})




if __name__ == '__main__':
    app.run()
